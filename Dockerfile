FROM python:3.9.5-alpine3.13

RUN apk update \
    && apk add --no-cache sshpass gcc g++ linux-headers libffi-dev openssl-dev openssh
RUN pip install pip --upgrade \
    && pip install cryptography==2.8 ansible \
    && apk del --no-cache libffi-dev openssl-dev


WORKDIR /workspace
RUN chown -R 1000:1000 .
COPY . .
RUN echo $AZURE_HOST > hosts

